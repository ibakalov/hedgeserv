import { Observable, Subject, takeUntil } from 'rxjs';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { COMMAND_TO_EXECUTION } from '@hedgeserv-shared/configs/command-to-execution.config';
import { CALC_DIRECTIONS } from '@hedgeserv-shared/enums/calc-commands.enum';
import { calculatorButtonsInterface } from '@hedgeserv-shared/interfaces/calculator-buttons.interface';
import { calculationValuesInterface } from '@hedgeserv-shared/interfaces/calculation-values.interface';
import { CalculatorService } from '@hedgeserv-web/shared/services/calculator.service';

@Component({
  selector: 'hedgeserv-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalculatorComponent implements OnInit {
  clickedButton: number;
  calculationValues: calculationValuesInterface = { firstValue: '', secondValue: '', command: null };
  currentValue: string = '';

  buttons$: Observable<Array<calculatorButtonsInterface> | undefined>;

  private _destroyed$ = new Subject<boolean>();

  constructor(private _calculatorService: CalculatorService) {}

  ngOnInit(): void {
    this.buttons$ = this._calculatorService.gatCalculatorData();
  }

  ngOnDestroy(): void {
    this._destroyed$.next(true);
    this._destroyed$.complete();
  }

  keyPress(event: Event | KeyboardEvent): void {
    if (event instanceof KeyboardEvent) {
      this.clickedButton = event.key.charCodeAt(0);
    }
  }

  detectClickedButton({ character, command }: Pick<calculatorButtonsInterface, 'character'> & Pick<calculatorButtonsInterface, 'command'>): void {
    if (command) {
      switch (command) {
        case CALC_DIRECTIONS.PARTIAL:
          this._partial();

          return;

        case CALC_DIRECTIONS.CONVERT_TO_PRECENT:
          this._convertToPrecent();

          return;

        case CALC_DIRECTIONS.CHANGE_POLARITY:
          this._changePolarity();

          return;

        case CALC_DIRECTIONS.SUBMIT:
          this._calculate();

          return;

        case CALC_DIRECTIONS.CLEAR:
          this._clearCalculator();

          return;
      }

      this.calculationValues.command = command;
    } else {
      if (typeof character === 'number') {
        const charecterToString = character.toString();

        if (charecterToString) this._setCurrentValue(character.toString(), this.currentValue === '0' ? true : false);
      }
    }
  }

  private _calculate(): void {
    const { firstValue, secondValue, command } = this.calculationValues;

    if (firstValue && secondValue && command && COMMAND_TO_EXECUTION[command] && typeof COMMAND_TO_EXECUTION[command] === 'function') {
      const result = command && COMMAND_TO_EXECUTION[command](+firstValue, +secondValue);

      this._calculatorService.setCalculationValues({ firstValue, secondValue, command }).pipe(takeUntil(this._destroyed$)).subscribe();

      this._clearCalculator();

      this._setCurrentValue(result !== undefined ? result.toString() : '');
    }
  }

  private _clearCalculator(): void {
    this.calculationValues = {
      firstValue: '',
      command: null,
      secondValue: '',
    };

    this.currentValue = '';
  }

  private _changePolarity(): void {
    if (this.currentValue) this._setCurrentValue((+this.currentValue * -1).toString(), true);
  }

  private _convertToPrecent(): number | void {
    if (this.currentValue) this._setCurrentValue((+this.currentValue * 0.01).toString(), true);
  }

  private _partial() {
    if (this.currentValue && this.currentValue.indexOf('.') === -1) this._setCurrentValue(this.currentValue + '.', true);
  }

  private _setCurrentValue(value: string, isNewValue = false) {
    const currentValueIndex: 'secondValue' | 'firstValue' = this.calculationValues.command ? 'secondValue' : 'firstValue';
    this.calculationValues[currentValueIndex] = (!isNewValue ? this.calculationValues[currentValueIndex] : '') + value;

    this.currentValue = this.calculationValues[currentValueIndex];
  }
}
