import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalculatorComponent } from './calculator.component';
import { CalculatorButtonsComponent } from './calculator-buttons/calculator-buttons.component';
import { CalculateButtonsHeightDirective } from './directives/calculate-buttons-height.directive';
import { CalculatorRoutingModule } from './calculator.routing.module';

@NgModule({
  declarations: [CalculatorComponent, CalculatorButtonsComponent, CalculateButtonsHeightDirective],
  imports: [CommonModule, CalculatorRoutingModule],
  exports: [CalculatorComponent],
})
export class CalculatorModule {}
