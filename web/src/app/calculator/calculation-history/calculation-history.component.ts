import { CALC_DIRECTIONS } from '@hedgeserv-shared/enums/calc-commands.enum';
import { Observable } from 'rxjs';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { calculationValuesInterface } from '@hedgeserv-shared/interfaces/calculation-values.interface';
import { CalculatorService } from '@hedgeserv-web/shared/services/calculator.service';

@Component({
  selector: 'hedgeserv-calculation-history',
  templateUrl: './calculation-history.component.html',
  styleUrls: ['./calculation-history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalculationHistoryComponent implements OnInit {
  calculationHistory$: Observable<Array<calculationValuesInterface> | undefined>;

  operationToSymbol: Partial<Record<string, unknown>> = {
    [CALC_DIRECTIONS.DIVISION]: 'Division',
    [CALC_DIRECTIONS.MULTIPLICATIONS]: 'Multiplications',
    [CALC_DIRECTIONS.SUBTRACTION]: 'Subtractions',
    [CALC_DIRECTIONS.ADDING]: 'Adding',
  };

  constructor(private _calculatorService: CalculatorService) {}

  ngOnInit(): void {
    this.calculationHistory$ = this._calculatorService.getCalculationValues();
  }
}
