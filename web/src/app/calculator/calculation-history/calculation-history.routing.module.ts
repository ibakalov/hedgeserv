import { CalculationHistoryComponent } from './calculation-history.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: CalculationHistoryComponent,
    title: 'Calculation history',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CalculationHistoryRoutingModule {}
