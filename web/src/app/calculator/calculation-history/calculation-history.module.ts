import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalculationHistoryComponent } from './calculation-history.component';
import { CalculationHistoryRoutingModule } from './calculation-history.routing.module';

@NgModule({
  declarations: [CalculationHistoryComponent],
  imports: [CommonModule, CalculationHistoryRoutingModule],
})
export class CalculationHistoryModule {}
