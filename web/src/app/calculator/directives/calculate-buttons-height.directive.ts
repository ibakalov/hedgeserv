import { AfterViewInit, Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[hedgeservCalculateButtonsHeight]',
})
export class CalculateButtonsHeightDirective implements AfterViewInit {
  @Input() elementsNumber: number;
  @Input() elementsInLine: number = 4;

  constructor(private _renderer: Renderer2, private _elementRef: ElementRef) {}

  ngAfterViewInit(): void {
    const elementWidth = (this._elementRef.nativeElement.offsetWidth / this.elementsInLine) * Math.ceil(this.elementsNumber / this.elementsInLine);

    this._renderer.setStyle(this._elementRef.nativeElement, 'height', `${elementWidth}px`);
  }
}
