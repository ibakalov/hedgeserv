import { CalculatorComponent } from './calculator.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: CalculatorComponent,
    title: 'Calculculator',
  },
  {
    path: 'calculator-history',
    loadChildren: (): Promise<any> => import('./calculation-history/calculation-history.module').then((m) => m.CalculationHistoryModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CalculatorRoutingModule {}
