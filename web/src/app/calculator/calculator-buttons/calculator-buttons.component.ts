import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { calculatorButtonsInterface } from '@hedgeserv-shared/interfaces/calculator-buttons.interface';

@Component({
  selector: 'hedgeserv-calculator-buttons',
  templateUrl: './calculator-buttons.component.html',
  styleUrls: ['./calculator-buttons.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalculatorButtonsComponent implements OnInit {
  @Input() set clickedButton(clickedButton: number) {
    if (clickedButton && clickedButton === this._buttonCharCode) {
      this.buttonClick();
    }
  }
  @Output() clickedButtonChange: EventEmitter<number> = new EventEmitter<number>();
  @Output() emitButtonClicked: EventEmitter<Pick<calculatorButtonsInterface, 'character'> & Pick<calculatorButtonsInterface, 'command'>> = new EventEmitter<
    Pick<calculatorButtonsInterface, 'character'> & Pick<calculatorButtonsInterface, 'command'>
  >();

  @Input() button: calculatorButtonsInterface;

  isButtonClick: boolean;

  private _buttonCharCode: number;

  ngOnInit(): void {
    this._buttonCharCode = this.button.customCKeyboardCharacter
      ? this.button.customCKeyboardCharacter.toString().charCodeAt(0)
      : this.button.character.toString().charCodeAt(0);
  }

  buttonClick(): void {
    this.isButtonClick = true;
    setTimeout(() => {
      this.isButtonClick = false;
      this.clickedButtonChange.emit(0);
    }, 100);

    const { command, character } = this.button;

    this.emitButtonClicked.emit({ command, character });
  }
}
