import { calculationValuesInterface } from '@hedgeserv-shared/interfaces/calculation-values.interface';
import { calculatorButtonsInterface } from '@hedgeserv-shared/interfaces/calculator-buttons.interface';
import { ResponseInterface } from '@hedgeserv-shared/interfaces/response.interface';
import { Observable } from 'rxjs';

export interface coalculatorServiceInterface {
  gatCalculatorData(): Observable<Array<calculatorButtonsInterface> | undefined>;
  setCalculationValues(value: calculationValuesInterface): Observable<ResponseInterface>;
  getCalculationValues(): Observable<Array<calculationValuesInterface> | undefined>;
}
