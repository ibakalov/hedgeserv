import { ResponseInterface } from '@hedgeserv-shared/interfaces/response.interface';
import { API_TOKEN } from '../injector-tokens/api-requests.token';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { catchError, EMPTY, map, Observable } from 'rxjs';
import { coalculatorServiceInterface } from './interfaces/calculator.interface';
import { calculatorButtonsInterface } from '@hedgeserv-shared/interfaces/calculator-buttons.interface';
import { calculationValuesInterface } from '@hedgeserv-shared/interfaces/calculation-values.interface';

@Injectable({
  providedIn: 'root',
})
export class CalculatorService implements coalculatorServiceInterface {
  constructor(private _http: HttpClient, @Inject(API_TOKEN) private _API_TOKEN: string) {}

  gatCalculatorData(): Observable<Array<calculatorButtonsInterface> | undefined> {
    return this._http.get<ResponseInterface<Array<calculatorButtonsInterface>>>(`${this._API_TOKEN}/get-calculator`).pipe(
      map((response) => response?.data),
      catchError(() => {
        return EMPTY;

        // Do something if error occure
      })
    );
  }

  setCalculationValues(value: calculationValuesInterface): Observable<ResponseInterface> {
    return this._http.post<ResponseInterface>(`${this._API_TOKEN}/calculation-history`, { value }).pipe(
      catchError(() => {
        return EMPTY;

        // Do something if error occure
      })
    );
  }

  getCalculationValues(): Observable<Array<calculationValuesInterface> | undefined> {
    return this._http.get<ResponseInterface<Array<calculationValuesInterface> | undefined>>(`${this._API_TOKEN}/calculation-history`).pipe(
      map((response) => response?.data),
      catchError(() => {
        return EMPTY;

        // Do something if error occure
      })
    );
  }
}
