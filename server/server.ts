import * as http from 'http';

import { router } from './router';
import { getCalculationHistory, setCalculationHistory } from './routes/calculation-history.route';
import { getCalculator } from './routes/get-calculator.route';

const hostname = '127.0.0.1';
const port = process.env.PORT || 3000;

const server = http.createServer((req: http.IncomingMessage, res: http.ServerResponse) => {
  router(req, res).get('/api/get-calculator', getCalculator);
  router(req, res).get('/api/calculation-history', getCalculationHistory);
  router(req, res).post('/api/calculation-history', setCalculationHistory);
});

server.on('error', (e) => {
  // Handle your error here
  console.log(e);
});

server.listen(+port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
