import { ServerResponse } from 'http';
import { readFile } from 'fs';
import { join } from 'path';
import { serverError } from '@hedgeserv-server/middleware/server-error.middleware';
import { calculatorButtonsInterface } from '@hedgeserv-shared/interfaces/calculator-buttons.interface';

const file = join(__dirname, '../', 'mock-data', 'calculator-data.mock.json');

export const getCalculator = (_: URL, res: ServerResponse) => {
  res.setHeader('Content-Type', 'application/json');

  readFile(file, 'utf-8', (err: unknown, rawData: string) => {
    if (err) {
      console.log(err);

      serverError(res);

      return;
    }

    try {
      const data: Array<calculatorButtonsInterface> = JSON.parse(rawData);

      res.end(JSON.stringify({ data }));
    } catch (error) {
      console.log(err);

      serverError(res);

      return;
    }
  });

  return;
};
