import { serverError } from '@hedgeserv-server/middleware/server-error.middleware';
import { createWriteStream, existsSync, readFile, writeFile } from 'fs';
import { ServerResponse } from 'http';

import { join } from 'path';

const file = join(__dirname, '../', 'mock-data', 'calculation-history.mock.txt');

if (!existsSync(file)) {
  console.log(file);
  writeFile(file, '', (err) => {
    if (err) console.warn(err);
  });
}

export const setCalculationHistory = (_: URL, res: ServerResponse, data?: Record<string, string>) => {
  const writeStreams = createWriteStream(file, {
    flags: 'a',
    encoding: 'utf-8',
  });
  res.setHeader('Content-Type', 'application/json');

  writeStreams.write(`${JSON.stringify(data?.value)} \n`, (err) => {
    if (err) console.log(err);
    writeStreams.end();
  });

  writeStreams.on('error', (err) => {
    console.log(err);

    return serverError(res);
  });

  res.end(JSON.stringify({ message: 'Ok' }));
};

export const getCalculationHistory = (req: URL, res: ServerResponse) => {
  res.setHeader('Content-Type', 'application/json');

  readFile(file, 'utf-8', (err: unknown, rawData: string) => {
    if (err) {
      console.log(err, req);

      serverError(res);

      return;
    }

    try {
      const data = rawData
        .split('\n')
        .splice(0, rawData.split('\n').length - 1)
        .map((values) => JSON.parse(values))
        .reverse();

      res.end(JSON.stringify({ data }));
    } catch (error) {
      console.log(err);

      serverError(res);

      return;
    }
  });

  return;
};
