import { notFound } from './routes/not-found.route';
import { IncomingMessage, ServerResponse } from 'http';
import { serverError } from './middleware/server-error.middleware';

export const router = (req: IncomingMessage, res: ServerResponse) => {
  if (req.url) {
    const reqUrl = new URL(req.url, 'http://127.0.0.1');

    return {
      get: (url: string, callback: (reqUrl: URL, res: ServerResponse) => unknown) => {
        if ((url === reqUrl.pathname || url === '*') && req.method === 'GET') {
          return callback(reqUrl, res);
        }
      },
      post: (url: string, callback: (reqUrl: URL, res: ServerResponse, data?: Record<string, string>) => unknown) => {
        if ((url === reqUrl.pathname || url === '*') && req.method === 'POST') {
          const chunks: Array<Uint8Array> = [];

          req.on('data', (chunk) => {
            chunks.push(chunk);
          });

          req.on('end', () => {
            try {
              return callback(reqUrl, res, JSON.parse(Buffer.concat(chunks).toString()));
            } catch (err) {
              console.log(err);

              return serverError(res);
            }
          });
        }
      },
    };
  }

  return {
    get() {
      return notFound(req, res);
    },
    post() {
      return notFound(req, res);
    },
  };
};
