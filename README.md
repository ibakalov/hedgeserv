# Hedgeserv

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.9.

## First things first

Run `npm install` into the main directory

# ANGULAR APP

## Development server

Run `ng serve` INTO THE WEB directory for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

# SERVER

## Run Server

Run `npm server:dev` into the main directory. If you have problems with the global instalation of webpack run
NODE_ENV=development npx webpack build --config ./server/webpack.config.js --watch & ./node_modules/nodemon/bin/nodemon.js server/dist/index.js

### Build

Run `npm server:buid` to make produyction build, but you still will need some sort of server to serve it.

#Important
The default port of the server is 3000 - the angular dev server proxy is made to point to that port when /api is called so it is neccessary to be runned on this port.
