import { CALC_DIRECTIONS } from '@hedgeserv-shared/enums/calc-commands.enum';
import { calcButtonsColorsType, calcButtonsSizesType } from '@hedgeserv-shared/types/calculator-buttons.type';

export interface calculatorButtonsInterface {
  character: string | number;
  command?: CALC_DIRECTIONS;
  color?: calcButtonsColorsType;
  size?: calcButtonsSizesType;
  customCKeyboardCharacter?: string;
}
