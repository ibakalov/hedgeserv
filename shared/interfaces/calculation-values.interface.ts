import { CALC_DIRECTIONS } from '@hedgeserv-shared/enums/calc-commands.enum';

export interface calculationValuesInterface {
  firstValue: string;
  command: CALC_DIRECTIONS | null;
  secondValue: string;
}
