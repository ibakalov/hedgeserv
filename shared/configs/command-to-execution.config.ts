import { CALC_DIRECTIONS } from '@hedgeserv-shared/enums/calc-commands.enum';
import { adding } from '@hedgeserv-shared/helpers/adding.helper';
import { division } from '@hedgeserv-shared/helpers/division.helper';
import { multiplication } from '@hedgeserv-shared/helpers/multiplication.helper';
import { subtraction } from '@hedgeserv-shared/helpers/subtraction.helper';

export const COMMAND_TO_EXECUTION: Partial<Record<string, any>> = {
  [CALC_DIRECTIONS.DIVISION]: division,
  [CALC_DIRECTIONS.MULTIPLICATIONS]: multiplication,
  [CALC_DIRECTIONS.SUBTRACTION]: subtraction,
  [CALC_DIRECTIONS.ADDING]: adding,
  [CALC_DIRECTIONS.SUBMIT]: division,
};
