export type calcButtonsColorsType = 'opposite08' | 'opposite05' | 'accent';

export type calcButtonsSizesType = 1 | 2;
